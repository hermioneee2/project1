import java.util.*;

/*
	Project 1
	Name of Project: 
	Author: Hye-rim Lee
	Student ID: 20180923
	Date: 2020-05-31
*/

MyText t;
NextButton toNextBtn;
ColorPicker cp1;
ColorPicker cp2;
ArrayList<Slider> sliders;
SizeSlider sizeSlider;
SpeedSlider speedSlider1;
SpeedSlider speedSlider2;
SpeedSlider speedSlider3;
Grid grid;

color bgcolor; 
int controlboxHeight;
int buttonX; //button x location
int buttonY; //button y location
int myState;

final color buttonColor = color (187, 134, 252);
final int buttonR = 30; //buttonsize
final int gridMinSize = 10; //minimum size of the grid

void setup(){
	size(800, 1000, P2D);
	controlboxHeight = height-width;
	buttonX = 10*width/11;
	buttonY = width+controlboxHeight/3*2;
	bgcolor = color(18, 18, 18); //initial background color
	myState = 1;
	
	t = new MyText();
	toNextBtn = new NextButton("Next", buttonX, buttonY, buttonR);
	toNextBtn.addObserver(t);

	cp1 = new ColorPicker(width/2-buttonR*7, buttonY-buttonR*3/2, buttonR*4, buttonR*3, 240);
	cp2 = new ColorPicker(width/2+buttonR*4, buttonY-buttonR*3/2, buttonR*4, buttonR*3, bgcolor);

	sliders= new ArrayList<Slider> ();
	sizeSlider = new SizeSlider (width/2-buttonR*7, buttonY, width/2, 16, 1, 15, 1000, 100);
	speedSlider1 = new SpeedSlider (width/2-buttonR*7, buttonY-buttonR, width/2, 16, 1, 0, 0.5, 0.05, 0);
	speedSlider2 = new SpeedSlider (width/2-buttonR*7, buttonY, width/2, 16, 1, 0, 0.5, 0.07, 1);
	speedSlider3 = new SpeedSlider (width/2-buttonR*7, buttonY+buttonR, width/2, 16, 1, 0, 150, 100, 2);
	sliders.add( sizeSlider );
	sliders.add( speedSlider1 );
	sliders.add( speedSlider2 );
	sliders.add( speedSlider3 );

	grid = new Grid();
}

//The order matter due to layer orders
void draw() {
	background(bgcolor);
	t.draw();

	//the motion (step 5)
	if (t.isReadyToMove()){
		t.copyAndReturnPGraphics();
		t.move(grid.getGridSize());
	}

	//adjust position of the text when moveable (step 3)
	if (t.isMoveable() && t.isMoveState()) {
		t.setLoc(mouseX, mouseY);
	}
	
	//show the grid size (step 4)
	if (grid.isAdjustState()) {
		grid.draw();
	}
	
	//draw controlbox\
	controlboxBasicDrawing(myState);

	//draw color palette & change background color (step 2)
	if (cp1.isVisible()){
		cp1.draw("Word");
		cp2.draw("Background");
		t.setC(cp1.getColor());
		bgcolor = cp2.getColor();
	}

	//adjust text size (step 3) + adjust movement (step 5)
	for (Slider s: sliders) {
		if (s.isVisible()){
			s.update();
			s.draw();
			s.registerText(t);
			s.adjustSize();
		}
	}

	toNextBtn.draw();
}

void controlboxBasicDrawing(int stateIndicator) {
	String [] description = {"Edit Text", "Color", "Size/Position", "Grid Size", "Movement" };
	int l = description.length;

	noStroke();
	fill(23);
	rect(0,width,width,controlboxHeight);
	noStroke();
	fill(50);
	rect(0,width+controlboxHeight/4,width,5);

	fill(50);
	textAlign(CENTER);
	textSize(18);
	for (int i=0; i<l; i++) {
		text(description[i], width/(l)*(i+0.5), width+controlboxHeight/6);
	}

	noStroke();
	fill(buttonColor);
	float myX = (stateIndicator-0.75)*width/l ;
	rect(myX, width+controlboxHeight/4, width/10, 5);
}

void keyPressed() {
	if (t.keyboardEnable()) {
		if (keyCode == BACKSPACE) {
			if (t.getText().length() > 0) {
				t.setText(t.getText().substring(0, t.getText().length()-1));
			}
		} else if (keyCode == 147) {
			t.setText("");
			t.setTsize(100);
		} else if (key != CODED && keyCode!=20) {
			t.setText(t.getText()+key);
		}
		// println(keyCode);
		// println(key);
	}
}

void mousePressed() {
	if (toNextBtn.isPressed()) {
		//println("it's pressed");
		toNextBtn.goNext();
	}

	if (t.isMoveState()) {
		if (t.isMoveable()) { // the mouse is on the view region
			t.position(false);
		} else {
			t.position(true);
		}
	}

	if (grid.isAdjustState()) {
		if (grid.isAdjustable()) {
			grid.adjustGrid(false);
		} else {
			grid.adjustGrid(true);
		}
	}	
}

//My major class "MyText"
public class MyText implements Observer{
	private PFont font;
	private String text;
	private float tsize;
	private color c;
	private PVector loc;
	private boolean enableKeyboard, moveableState, adjustPosition, readyToMove;
	private PGraphics pg;
	private float[] moveVariables;
	DecisionState state;

	MyText() {
		this.tsize = 40;
		this.font = createFont("Muli.ttf", tsize);
		this.text = "Type your word here.\nPress 'delete' to clear.";
		this.c = 240;
		this.loc = new PVector(width/2, width/2);
		state = new TextDecided();
		enableKeyboard = true;
		moveableState = false;
		adjustPosition = false;
		readyToMove = false;
		moveVariables = new float[] {0.05, 0.07, 100};
	}

	//Setters and getters
	void setfont(String newFont) {
		this.font = createFont(newFont, tsize);
	}

	void setText(String newText) {
		this.text = newText;
	}

	void setTsize(float newTsize) {
		this.tsize = newTsize;
	}

	void setC(color newC) {
		this.c = newC;
	}

	void setLoc(float x, float y) {
		this.loc = new PVector(x, y);
	}

	String getText() {
		return text;	
	}

	//regarding to the editable states
	void disableKeyboard() {
		enableKeyboard = false;
	}

	boolean keyboardEnable() {
		return enableKeyboard;
	}

	void moveState(boolean b) {
		moveableState=b;
	}

	void position(boolean b) {
		adjustPosition=b;
	}

	boolean isMoveState() {
		return moveableState && (mouseY < width);
	}

	boolean isMoveable() {
		return adjustPosition  && (mouseY < width);
	}

	void readyItToMove(boolean b){
		readyToMove = b;
	}

	boolean isReadyToMove(){
		return readyToMove;
	}

	void setMoveVariable(float a, int i) {
		moveVariables[i] = a;
	}
	
	//display the text
	void draw() {
		pg = createGraphics(width, width);
		pg.beginDraw();
		pg.background(bgcolor); //global
		pg.fill(this.c);
		pg.textFont(this.font);
		pg.textSize(this.tsize);
		pg.pushMatrix();
		pg.translate(this.loc.x, this.loc.y);
		pg.textAlign(CENTER, CENTER);
		pg.text(this.text, 0, 0);
		pg.popMatrix();
		pg.endDraw();
		image(pg,0,0);
	}
	
	//getting ready for movement
	void copyAndReturnPGraphics () {
		pg = createGraphics(width, width);
		pg.beginDraw();
		pg.background(bgcolor); //global
		pg.fill(this.c);
		pg.textFont(this.font);
		pg.textSize(this.tsize);
		pg.pushMatrix();
		pg.translate(this.loc.x, this.loc.y);
		pg.textAlign(CENTER, CENTER);
		pg.text(this.text, 0, 0);
		pg.popMatrix();
		pg.endDraw();
		copy(pg,0,0,800,800,0,0,800,800); //testing
		//pg.save("practice.png");
		//return pg;
	}

	void move(PVector box){
		int tileW = int(box.x);
		int tileH = int(box.y);

		int tilesX = int(width/tileW);
		int tilesY = int(height/tileH);

		float v1=moveVariables[0];
		float v2=moveVariables[1];
		float v3=moveVariables[2];

		for (int y = 0; y < tilesY; y++) {
			for (int x = 0; x < tilesX; x++) {
				// WARP
				int wave = int(sin(frameCount * v1 + ( x * y ) * v2) * v3);

				// SOURCE
				int sx = x*tileW + wave;
				int sy = y*tileH;
				int sw = tileW;
				int sh = tileH;

				// DESTINATION
				int dx = x*tileW;
				int dy = y*tileH;
				int dw = tileW;
				int dh = tileH;
				
				copy(pg, sx, sy, sw, sh, dx, dy, dw, dh);
			}
		}
	}

	void setState (DecisionState s) {
		this.state=s;
	}

	public void update(Observable obs, Object param) {
		if (!(state instanceof MovementDecided)) {
			state.nextStep(this);
		}
	}
}

//button goes to next step
class NextButton extends Observable {
	private int x, y;
	private color col;
	private int r;
	private String id;

	NextButton (String id, int x, int y, int r) {
		this.x= x; this.y= y; this.id= id; this.r= r;
		col= buttonColor;
	}

	boolean isPressed() {
		int dx= mouseX-x;
		int dy= mouseY-y;
		return sqrt(dx*dx + dy*dy) < r;
	}

	void draw() {
		ellipseMode(CENTER);
		fill(col);
		noStroke();
		ellipse (x,y,r*2,r*2);
		stroke(23);
		strokeWeight(5);
		line(x-r/6, y-r/2.5, x+r/4, y);
		strokeWeight(5);
		line(x-r/6, y+r/2.5, x+r/4, y);
	}

	void goNext(){
		setChanged();
		notifyObservers(id);
		//println("goNext");
	}
}

//decision state:  1. text - 2. color - 3. size/position - 4. grid size - 5. move
interface DecisionState {
	void nextStep(MyText myText);
}

class TextDecided implements DecisionState {
	void nextStep(MyText myText) {
		//println("text decided");
		myState=2;
		t.disableKeyboard();
		cp1.setVisible(true);
		cp2.setVisible(true);
		myText.setState (new ColorDecided());
	}
}

class ColorDecided implements DecisionState {
	void nextStep(MyText myText) {
		myState=3;
		cp1.setVisible(false);
		cp2.setVisible(false);
		t.moveState(true);
		sizeSlider.setVisible(true);
		//println("color decided");
		myText.setState (new ArrangementDecided());
	}
}

class ArrangementDecided implements DecisionState {
	void nextStep(MyText myText) {
		myState=4;
		t.moveState(false);
		sizeSlider.setVisible(false);
		grid.adjustState(true);
		//println("arrangement decided");
		myText.setState (new GridDecided());
	}
}

class GridDecided implements DecisionState {
	void nextStep(MyText myText) {
		myState=5;
		grid.adjustState(false);
		t.readyItToMove(true);
		speedSlider1.setVisible(true);
		speedSlider2.setVisible(true);
		speedSlider3.setVisible(true);
		//println("grid decided");
		myText.setState (new MovementDecided());
	}
}

class MovementDecided implements DecisionState {
	void nextStep(MyText myText) { 
	}
}

//Step 2 color picker
class ColorPicker {
	private int x, y, w, h, c;
	private PImage cpImage;
	private boolean visible;

	public ColorPicker ( int x, int y, int w, int h, color c) {
		this.x = x; this.y = y; this.w = w; this.h = h; this.c = c;
		cpImage = new PImage( w, h );
		visible = false;
		init();
	}

	private void init () {
		// draw color
		int cw = h;
		for( int i=0; i<cw; i++ ) {
			float nColorPercent = i / (float)cw;
			float rad = (-360 * nColorPercent) * (PI / 180);
			int nR = (int)(cos(rad) * 127 + 128) << 16;
			int nG = (int)(cos(rad + 2 * PI / 3) * 127 + 128) << 8;
			int nB = (int)(Math.cos(rad + 4 * PI / 3) * 127 + 128);
			int nColor = nR | nG | nB;

			setGradient( i, 0, 1, h/2, 0xFFFFFF, nColor );
			setGradient( i, (h/2), 1, h/2, nColor, 0x000000 );
		}

		// draw black/white
		// drawRect( cw, 0,  h+5, h/2, 0xFFFFFF );
		// drawRect( cw, h/2, h+5, h/2, 0 );

		// draw grey scale
		for( int j=0; j<h; j++ ) {
			int g = 255 - (int)(j/(float)(h-1) * 255 );
			drawRect( w-20, j, h+5, 1, color( g, g, g ) );
		}
	}

	private void setGradient(int x, int y, float w, float h, int c1, int c2 ) {
		float deltaR = red(c2) - red(c1);
		float deltaG = green(c2) - green(c1);
		float deltaB = blue(c2) - blue(c1);

		for (int j = y; j<(y+h); j++) {
			int c = color( red(c1)+(j-y)*(deltaR/h), green(c1)+(j-y)*(deltaG/h), blue(c1)+(j-y)*(deltaB/h) );
			cpImage.set( x, j, c );
		}
	}

	private void drawRect( int rx, int ry, int rw, int rh, int rc ) {
		for(int i=rx; i<rx+rw; i++) {
			for(int j=ry; j<ry+rh; j++) {
				cpImage.set( i, j, rc );
			}
		}
	}

	public void draw(String colorname) {
		image( cpImage, x, y );
		if( mousePressed &&
			mouseX >= x && 
			mouseX < x + w &&
			mouseY >= y &&
			mouseY < y + h ) {
			c = get( mouseX, mouseY );
		}
		fill( c );
		rect( x-w-h/2, y+h-30, w+h/2-15, 30 );
		
		fill(buttonColor);
		textSize(20);
		textAlign(LEFT);
		text(colorname, x-w-h/2+5, y+15);
	}

	color getColor() {
		return c;
	}

	void setVisible(boolean b) {
		visible = b;
	}

	boolean isVisible() {
		return visible;
	}
}

//Step 4 adjust grid size
class Grid {
	private int boxX;
	private int boxY;
	private boolean adjustable, adjustState;

	Grid () {	
		adjustable = false;
		adjustState = false;
	}

	void mapToAppropriateSize() {
		if (adjustable==true) {
			boxX=mouseX;
			boxY=mouseY;
		}

    if (boxX<gridMinSize) boxX=gridMinSize;
    if (boxY<gridMinSize) boxY=gridMinSize;
	}

	void draw() {
  	stroke(buttonColor);
  	strokeWeight(2);	 
		mapToAppropriateSize();
		if (this.isOverTheRegion() || !adjustable) {
			if ((width%boxX)!=0) {
				boxX = width/(width/boxX);
			}

			if ((width%boxY)!=0) {
				boxY = width/(width/boxY);
			}
		
			for (int i=1; i<(width/boxX); i++) {
				line(boxX*i, 0, boxX*i, width);
			}
			for (int i=1; i<(width/boxY); i++) {
				line(0, boxY*i, width, boxY*i);
			}
		}
	}

	void adjustGrid(boolean b) {
		adjustable=b;
	}

	boolean isAdjustable() {
		return adjustable;
	}

	void adjustState(boolean b) {
		adjustState=b;
	}

	boolean isAdjustState() {
		return adjustState;
	}

	//preventing division by zero
	boolean isOverTheRegion(){
		if (mouseX < 0 || mouseX > width) return false;
		if (mouseY < 0 || mouseY > width) return false;
		return true;
	}

	PVector getGridSize() {
		return new PVector(boxX, boxY);
	}
}

//Step 3 and 5 slider
abstract class Slider {
  protected float swidth, sheight;    // width and height of bar
  protected float xpos, ypos;       // x and y position of bar
  protected float spos, newspos;    // x position of slider
  protected float sposMin, sposMax; // max and min values of slider
  protected float loose;              // how loose/heavy
  protected boolean over;           // is the mouse over the slider?
  protected boolean locked;
  protected float ratio;
	protected boolean visible;
	protected MyText txt;
	protected float a; //minsize
	protected float b; //maxsize
	protected float ix; //initial size
	//sizeSlider = new SizeSlider (width/2-buttonR*7, buttonY, width/2, 16, 1, 15, 1000, 100);

  Slider (float xp, float yp, float sw, float sh, float l, float a, float b, float ix) {
    swidth = sw;
    sheight = sh;
    float widthtoheight = sw - sh;
    ratio = (float)sw / (float)widthtoheight;
    xpos = xp;
    ypos = yp-sheight/2;		
    sposMin = xpos;
    sposMax = xpos + swidth - sheight;
    loose = l;
		visible = false;
		this.a=a;
		this.b=b;
		spos=(ix-a)/(b-a)*(swidth - sheight)+sposMin;
		newspos = spos;
  }

  void update() {
    if (overEvent()) {
      over = true;
    } else {
      over = false;
    }
    if (mousePressed && over) {
      locked = true;
    }
    if (!mousePressed) {
      locked = false;
    }
    if (locked) {
      newspos = constrain(mouseX-sheight/2, sposMin, sposMax);
    }
    if (abs(newspos - spos) > 1) {
      spos = spos + (newspos-spos)/loose;
    }
  }

  float constrain(float val, float minv, float maxv) {
    return min(max(val, minv), maxv);
  }

  boolean overEvent() {
    if (mouseX > xpos && mouseX < xpos+swidth &&
       mouseY > ypos && mouseY < ypos+sheight) {
      return true;
    } else {
      return false;
    }
  }

  void draw() {
    noStroke();
    fill(30);
    rect(xpos, ypos, swidth, sheight);
    if (over || locked) {
      fill(159, 82, 255);
    } else {
      fill(buttonColor);
    }
    rect(spos, ypos, sheight, sheight);
  }

	float getPos() {
		float mysize= (spos-sposMin)/(sposMax-sposMin)*(b-a)+a;
		return mysize;
  }

	abstract void adjustSize();

  void setVisible(boolean b) {
		visible = b;
	}

  boolean isVisible() {
		return visible;
	}		

	void registerText(MyText t) {
		txt=t;
	}
}

//Step 3
class SizeSlider extends Slider{
  SizeSlider (float xp, float yp, float sw, float sh, float l, float a, float b, float ix) {
		super ( xp,  yp,  sw,  sh, l, a, b, ix);
	}

	@Override
	void adjustSize() {
		txt.setTsize(super.getPos());
	}
}

//Step 5
class SpeedSlider extends Slider{
	private int index;

  SpeedSlider (float xp, float yp, float sw, float sh, float l, float a, float b, float ix, int index) {
		super ( xp,  yp,  sw,  sh, l, a, b, ix);
		this.index = index;
	}

	@Override
	void adjustSize() {
		txt.setMoveVariable(super.getPos(), index);
	}
}