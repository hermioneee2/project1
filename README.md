# PR1: Playful Text

# Playful Text Project

### Table of content

- Project 1 template: [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing)
- [Source code](vscode-resource://file///c%3A/Users/%EC%9D%B4%ED%98%9C%EB%A6%BC/Documents/GitHub/project1/Project1_Code/)
- [Demo video](vscode-resource://file///c%3A/Users/%EC%9D%B4%ED%98%9C%EB%A6%BC/Documents/GitHub/project1/Video/)
- Description of projects and notes in [README.md](http://readme.md/)(this file).

## Description of the Project

The "Playful Text" allows you to play around with the text, by modifying several features and giving movement at the end. The programs follows 5 steps below, and each step can't be reverted, so be sure to choose the right feature you want! The control system on the bottom of the screen will show you your state, so check out how far you've got :)

1. **Write your text.** You can delete the whole text in the beginning by simply pressing the delete button, and directly type words into the screen. If you are done with the text, click next button on the bottom right corner.
2. **Choose the color of your text, and the background.** You can click the colors from the bottom control bar, and it will automatically apply to your text. Play around until you got the right color!
3.  **Arrange your text by changing its position and size.** The slider on the control system will resize your text. Click the screen to move the position of the text, and click again to fix the position. If you want to change the position again, just click again!
4. **Decide the size of the box that will affect the movement visualization.** Hover over screen then it will show the size of the grid that you are going to use in the movement part. Smaller or larger, decide how you like. 
5. **Move your text and play around.** Movement can be adjusted by three slider on the bottom. Move around until you get the one that you like!

## Inspirations and References

### Inspirations

- Initially inspired by kinetic typography "5" from travismillerdesign [http://travismillerdesign.com/experiments/GEN_CAPS/](http://travismillerdesign.com/experiments/GEN_CAPS/)
- The "programming poster" by tim rodenbröker creative coding with simple typography and movement inspired me.
[https://www.youtube.com/watch?v=tiwq4hjaKEM](https://www.youtube.com/watch?v=tiwq4hjaKEM)
- Simple color theme is inspired from this(design side)
[https://blog.prototypr.io/how-to-design-a-dark-theme-for-your-android-app-3daeb264637](https://blog.prototypr.io/how-to-design-a-dark-theme-for-your-android-app-3daeb264637)

### Dependencies

To keep the look and feel of UI side, instead of using GUI library directly, I took following codes from these sources and modified a bit.

- ColorPicker: The display of color picker such as "gradient" and "draw" is mostly referred from this code. Additional functions added by myself are getColor(), setVisible(), and isVisible(). The size and the arrangement of the color picker are modified quite a lot to fit the UI of Playful Text.

    [https://forum.processing.org/one/topic/processing-color-picker.html](https://forum.processing.org/one/topic/processing-color-picker.html)

- Slider: The abstract class slider in this project is mostly from following source, as well as all the long variables inputs. What I added are variables such as a, b, ix, to map the current location of the slider to the desired value such as from 15pt to 1000pt for the size(they are mathematically calculated). I also added adjustSize functions for the sliders that extends the slider class above, to connect the slider with the text using observer design pattern.

    [https://processing.org/examples/scrollbar.html](https://processing.org/examples/scrollbar.html)

- Movement: The move() function of the text is mostly referred from the code from following source. Additional lookup was done to understand copy function. Some variables such as tileX and tileY were replaced with my own variables, to make connection with the grid class in my code.

    [https://timrodenbroeker.de/processing-tutorial-kinetic-typography-1/](https://timrodenbroeker.de/processing-tutorial-kinetic-typography-1/)

    [https://processing.org/reference/copy_.html](https://processing.org/reference/copy_.html)

### References

- Keyboard input: when handling the keyboard input and display, this source was referred.

    [https://forum.processing.org/one/topic/keyboard-input-to-change-a-string-variable.html](https://forum.processing.org/one/topic/keyboard-input-to-change-a-string-variable.html)

- The "snippet" codes from ID311: to refer to design patterns such as MVC, state machine, and observer and usage of classes, inheritance, polymorphism.

## Rooms for Improvements

- Clear instructions on the program itself.
- Additional features such as allowing to input color codes of RGB or size of the font.
- Reversing buttons.
- Allowing to change several fonts.
- Allowing to load images and use it as background.
- Supporting export video function for the motion that you created.